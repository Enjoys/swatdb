# SwatDB

Simple Wrapper at DB (based on PDO)

Изначально (много лет назад) это был форк библиотеки [DbSimple](https://github.com/DmitryKoterov/DbSimple), сейчас от
нее остались только регулярки для кастомных плейсхолдеров.

# Generals (notes)

## Select

### Метод select()

Это обертка над PDO::prepare и PDOStatement:execute

```php
/** @var Enjoys\SwatDB\Wrapper $db */
$sth = $db->select("SELECT * FROM ..."); //return PDOStatement
//далее 
$sth->fetchAll(); //и т.д.
```

### Декорирующие select-методы

Возвращают сразу результат, а не PDOStatement

```php
/** @var Enjoys\SwatDB\Wrapper $db */
$db->selectAll("SELECT * FROM ..."); //return array
$db->selectArr("SELECT id, name FROM ..."); //return array keypair id => name
$db->selectCell("SELECT COUNT(*) FROM ..."); //return int|string value
$db->selectCol("SELECT colname FROM ..."); //return array<int, string>
```

### Кэширование результатов декорирующих select-методов

Таких как, *selectAll()*, *selectArr()*, *selectCell()*, *selectCol()*, *selectRow()*

```php
use Enjoys\SimpleCache\Cacher;
$db = Enjoys\SwatDB\DB::connect([
    'dsn' => 'sqlite:memory',
    //Любой cacher реализующий интерфейс Psr\SimpleCache\CacheInterface
    'cacher' => new Cacher\Memcached() 
]);

//Альтернативная установка cacher
$db->setCacher(new Cacher\FileCache());

$db->selectAll(300, "SELECT * FROM ..."); //кэширование на 5 минут
//аналогично и на другие методы
```

## Insert

Вставка одной строки

```php
/** @var Enjoys\SwatDB\Wrapper $db */
$db->insert('table_name', ['column' => 'value', ...]); //Returns the number of rows affected
```

MySQL: Вставка строки вместе с ON DUPLICATE KEY UPDATE (UPSERT) простой вариант

```php
/** @var Enjoys\SwatDB\Wrapper $db */
//INSERT INTO table_name SET (`column`, ...) VALUE ('value', ...) ON DUPLICATE KEY UPDATE column = ?, ...
$db->upsert('table_name', ['column' => 'value', ...]); //Returns the number of rows affected
```

SQLite: ON CONFLICT(x) DO UPDATE SET (самый простой вариант)
Единственное условие, в массиве с данными, первым должен быть ключ или PRIMARY или UNIQUE

```php
/** @var Enjoys\SwatDB\Wrapper $db */
//INSERT INTO table_name SET (`column`, ...) VALUE ('value', ...) ON CONFLICT(`column`) DO UPDATE SET column = ?, ...
$db->upsert('table_name', ['column' => 'value', ...]); //Returns the number of rows affected
```

Вставка нескольких строк INSERT INTO table (column, ...) VALUES (value1, ...), (value2, ...)

```php
/** @var Enjoys\SwatDB\Wrapper $db */
$db->insertMulti('table_name', ['column1', ...], [['value1', ...], ['value2', ...]]); //Returns the number of rows affected

$columns = ['name', 'email'];
$values = [
    ['Вася', 'vasya@example.com'],
    ['Федя', 'fedya@example.com'],
];
//INSERT INTO table_name (name, email) VALUES ('Вася', 'vasya@example.com'), (Федя', 'fedya@example.com')
$cnt = $db->insertMulti('table_name', $columns, $values); //$cnt = 2

```

```php
/** @var Enjoys\SwatDB\Wrapper $db */
//$db->replace(); in future versions
```

## Update

### Простые сценарии UPDATE можно вызывать через метод *update()*

UPDATE table SET ... WHERE ...

```php
/** @var Enjoys\SwatDB\Wrapper $db */
//SQL: UPDATE table_name SET 'mycolumn' = `updated` WHERE `id` = 1
$db->update('table_name', ['mycolumn' => 'updated'], 'id = ?d', 1); //Returns the number of rows affected

```

## Delete

### Простые сценарии DELETE можно вызывать через метод *delete()*

```php
/** @var Enjoys\SwatDB\Wrapper $db */
//DELETE FROM table WHERE ...
$db->delete('table_name', 'id = ?d', 10); //Returns the number of rows affected
```
## Truncate
```php
/** @var Enjoys\SwatDB\Wrapper $db */
//DELETE FROM table WHERE ...
$db->truncate('table_name'); //Work in MySQL, SQLite
```

## Create, Drop, ... и тд

Пока это не реализовано, все в планах, сейчас лучше использовать *exec* для этих целей

```php
/** @var Enjoys\SwatDB\Wrapper $db */
//$db->create(); //TODO
//$db->drop(); //TODO
//$db->truncate(); //TODO

```