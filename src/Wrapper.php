<?php

declare(strict_types=1);

namespace Enjoys\SwatDB;

use Enjoys\SwatDB\Interfaces\Cacheable;
use Enjoys\SwatDB\Interfaces\DriverInterface;
use Enjoys\SwatDB\Interfaces\MethodInterface;
use Enjoys\SwatDB\Interfaces\StatsInterface;
use Enjoys\Traits\Options;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Wrapper
 * @package Enjoys\SwatDB
 * @method false|\PDOStatement select(string $query, mixed ...$arguments)  @see \Enjoys\SwatDB\Methods\Select
 * @method array selectAll(int|string $query, mixed ...$arguments)  @see \Enjoys\SwatDB\Methods\SelectAll  Если первый аргумент int ($cacheLifeTime), то второй аргумент должен быть ОБЯЗАТЕЛЬНО string ($query)
 * @method false|int|float|string selectCell(int|string $query, mixed ...$arguments)  @see \Enjoys\SwatDB\Methods\SelectCell  Если первый аргумент int ($cacheLifeTime), то второй аргумент должен быть ОБЯЗАТЕЛЬНО string ($query)
 * @method false|array selectCol(int|string $query, mixed ...$arguments)  @see \Enjoys\SwatDB\Methods\SelectCol  Если первый аргумент int ($cacheLifeTime), то второй аргумент должен быть ОБЯЗАТЕЛЬНО string ($query)
 * @method false|array selectRow(int|string $query, mixed ...$arguments)  @see \Enjoys\SwatDB\Methods\SelectRow  Если первый аргумент int ($cacheLifeTime), то второй аргумент должен быть ОБЯЗАТЕЛЬНО string ($query)
 * @method false|array selectArr(int|string $query, mixed ...$arguments)  @see \Enjoys\SwatDB\Methods\SelectArr  Если первый аргумент int ($cacheLifeTime), то второй аргумент должен быть ОБЯЗАТЕЛЬНО string ($query)
 * @method false|int insert(string $string, mixed $data) @see \Enjoys\SwatDB\Methods\Insert
 * @method false|int insertMulti(string $query, array $columns, array $values) @see \Enjoys\SwatDB\Methods\InsertMulti
 * @method false|int upsert(string $table, array $data)  @see \Enjoys\SwatDB\Methods\Upsert
 * @method false|int update(string $table, array $data, string $where, mixed ...$arguments) @see \Enjoys\SwatDB\Methods\Update
 * @method false|int delete(string $table, string $where, mixed ...$arguments) @see \Enjoys\SwatDB\Methods\Delete
 * @method false|int truncate(string $table) @see \Enjoys\SwatDB\Methods\Truncate
 */
abstract class Wrapper extends \PDO implements DriverInterface
{
    use Options;

    public const SKIP = -INF;
    public const ERROR_VALUE_NOT_ARRAY = 'Значение должно быть массивом';
    public const ERROR_VALUE_NOT_SCALAR = 'Значение должно быть скалярным  (int, float, string или bool)';
    public const ERROR_NO_VALUE = 'Не передано значения для подстановки (bind)';

    /**
     * @var StatsInterface
     */
    private StatsInterface $stats;

    private ?string $cacheId = null;

    /**
     * @var mixed
     */
    protected $version = null;
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    /**
     * Wrapper constructor.
     *
     * @param array<mixed> $options
     * <i>dsn</i> string The Data Source Name, or DSN, contains the information required to connect to the database.<br>
     * <i>username</i> string The user name for the DSN string. This parameter is optional for some PDO drivers.<br>
     * <i>password</i> string The password for the DSN string. This parameter is optional for some PDO drivers.<br>
     * <i>pdoOptions</i> array A key=>value array of driver-specific connection options.<br>
     * <i>tablePrefix</i> string Prefix table use as like ?_table_name<br>
     * <i>statsLogQuery<i> bool Write to log Stats Class query, default false
     * @param LoggerInterface|null $logger
     * @param StatsInterface|null $stats
     * @throws Exception
     */
    public function __construct(array $options = [], LoggerInterface $logger = null, StatsInterface $stats = null)
    {
        $this->setOptions($options, false);

        $this->logger = $logger ?? new NullLogger();
        $this->stats = $stats ?? new Stats($this->getOption('statsLogQuery', false));

        try {
            parent::__construct(
                $this->getOption('dsn', ''),
                $this->getOption('username'),
                $this->getOption('password'),
            );

            $this->setPdoAttributes($this->getOption('pdoOptions', [], false));
        } catch (\PDOException $e) {
            throw new Exception($this->logger, $e->getMessage());
        }
    }

    /**
     * @param array<mixed> $attributes
     */
    private function setPdoAttributes(array $attributes = []): void
    {

        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        //Set custom Statement Class
        $this->setAttribute(\PDO::ATTR_STATEMENT_CLASS, [Statement::class, [$this->logger, $this->stats]]);

        foreach ($attributes as $attribute => $value) {
            $this->setAttribute($attribute, $value);
        }
    }

    /**
     * Sets a logger.
     *
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }


    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }


    /**
     * @return mixed
     */
    public function getStats()
    {
        return $this->stats->getStats(...func_get_args());
    }

    /**
     * @return string
     */
    public function getCacheId(): ?string
    {
        return $this->cacheId;
    }

    /**
     * Get Table Prefix
     * @return string
     */
    public function getTablePrefix(): string
    {
        return $this->getOption('tablePrefix', '', false);
    }


    /**
     * @param string $name
     * @param array<mixed> $args
     * @return mixed
     * @throws Exception
     */
    public function __call(string $name, array $args)
    {
        $method = $this->getCallMethod($name, $args);

        try {
            if (!($method instanceof MethodInterface)) {
                throw new Exception(
                    $this->logger,
                    sprintf(
                        'Method %s not implements %s',
                        get_class($method),
                        MethodInterface::class
                    )
                );
            }

            if ($method instanceof Cacheable) {
                $method->setCacher($this->getOption('cacher'));
                $this->cacheId = $method->getCacheId();
            }
            return $method->exec();
        } catch (\PDOException $e) {
            $this->rollBack();
            throw new Exception($this->logger, $e->getMessage());
        }
    }

    /**
     * @param string $method
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    private function getCallMethod(string $method, array $args)
    {
        $class = "Enjoys\SwatDB\Methods\\" . \ucfirst($method);
        if (!class_exists($class)) {
            throw new Exception(
                $this->logger,
                sprintf('Method %s undefined', get_called_class() . '::' . $method . '()')
            );
        }

        return new $class($this, ...$args);
    }


    #[\ReturnTypeWillChange]
    /**
     * Декорирует оригинальный метод PDO::rollBack
     * в отличии от оригинального метода, тут идет проверка на активную транзакцию,  и если ее нет, просто вернет false,
     * а не выбросит исключение
     * @return bool
     * @throws \PDOException if there is no active transaction.
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function rollBack()
    {
        if ($this->inTransaction()) {
            return parent::rollBack();
        }
        return false;
    }


    #[\ReturnTypeWillChange]
    /**
     * Декорирует оригинальный метод PDO::exec
     * Выполняет SQL-запрос и возвращает количество затронутых строк
     * Для удобства добавлен ?_ префиксный плейсхолдер
     *
     * @param string $statement запрос к БД
     * @return false|int
     * @throws Exception
     * @noinspection PhpMissingParamTypeInspection
     */
    public function exec($statement)
    {
        return $this->wrapExecQuery('exec', $statement);
    }

    #[\ReturnTypeWillChange]
    /**
     * Декорирует оригинальный метод PDO::query
     * Выполняет SQL-запрос и возвращает результирующий набор в виде объекта PDOStatement
     * Для удобства добавлен ?_ префиксный плейсхолдер
     *
     * @param string $query
     * @param int|null $fetchMode
     * @param mixed ...$fetchModeArgs
     * @return false|\PDOStatement
     * @throws Exception
     * @noinspection PhpSignatureMismatchDuringInheritanceInspection
     */
    public function query(string $query, ?int $fetchMode = null, ...$fetchModeArgs)
    {
        return $this->wrapExecQuery('query', $query);
    }

    /**
     * @param string $command
     * @param string $statement
     * @return mixed
     * @throws Exception
     */
    private function wrapExecQuery(string $command, string $statement)
    {
        try {
            $start_time = microtime(true);

            $query = (new Prepare($this))->send($statement)->getQuery();

            $result = parent::$command($query);

            $end_time = microtime(true) - $start_time;

            $this->logger->info(
                '[' . $end_time . '] PDO::' . $command . ' - ' . $query,
                [
                    'query' => $query,
                    'time' => $end_time,
                ]
            );

            $this->stats->addToQueryTime($end_time);
            $this->stats->addToQueryCount();
            $this->stats->addToQueryList($query);

            return $result;
        } catch (\PDOException $e) {
            $this->rollBack();
            throw new Exception($this->logger, $e->getMessage());
        }
    }
}
