<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Drivers;

use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Interfaces\StatsInterface;
use Enjoys\SwatDB\Prepare;
use Enjoys\SwatDB\Wrapper;
use Psr\Log\LoggerInterface;

/**
 * Class Sqlite
 * @package Enjoys\SwatDB\Drivers
 */
final class Sqlite extends Wrapper
{

    /**
     * Sqlite constructor.
     * @param array<mixed> $options
     * @param LoggerInterface|null $logger
     * @param StatsInterface|null $stats
     * @throws Exception
     */
    public function __construct(array $options = [], LoggerInterface $logger = null, StatsInterface $stats = null)
    {
        parent::__construct($options, $logger, $stats);

        if ($sth = $this->query('SELECT sqlite_version();')) {
            $this->version = $sth->fetchColumn();
        }
    }

    /**
     * Экранирует строки
     * @param string $s
     * @param bool $isIdentifier
     * @return string
     */
    public function escape(string $s, $isIdentifier = false): string
    {
        if (!$isIdentifier) {
            return $this->quote($s);
        } else {
            return "`" . str_replace('`', '``', $s) . "`";
        }
    }


    /**
     * @inheritDoc
     */
    public function performGetPlaceholderIgnoreRe(): string
    {
        return '';
    }

    /**
     * Это простая реализация , выбирает первую колонку в качестве уникального значения
     * @param string $table
     * @param array<mixed> $data
     * @return false|int
     * @throws Exception
     * @since 1.4.1
     */
    public function upsert(string $table, array $data)
    {
        if (version_compare($this->version, '3.24.0') < 0) {
            throw new Exception($this->logger, sprintf('Driver %s not support UPSERT', get_called_class()));
        }
        $table = $this->escape($table, true);

        $columns = array_keys($data);
        $values = array_values($data);

        $query = "INSERT INTO {$table} (?#) VALUES(?a) ON CONFLICT(?#) DO UPDATE SET ?a";

        $prepare = new Prepare($this);

        /** @var \PDOStatement|false $sth */
        $sth = $prepare->send($query, $columns, $values, $columns[0], $data)->execute();

        if ($sth === false) {
            return false;
        }

        return $sth->rowCount();
    }

    /**
     * @param string $table
     * @return false|int
     * @throws Exception
     * @since 1.6.2
     */
    public function truncate(string $table)
    {
        $table = $this->escape($table, true);
        return $this->exec("DELETE FROM {$table}; VACUUM;");
    }
}
