<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Drivers;

use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Interfaces\StatsInterface;
use Enjoys\SwatDB\Prepare;
use Enjoys\SwatDB\Wrapper;
use Psr\Log\LoggerInterface;

/**
 * Class Mysql
 * @package Enjoys\SwatDB\Drivers
 */
final class Mysql extends Wrapper
{


    /**
     * Mysql constructor.
     * @param array<mixed> $options
     * @param LoggerInterface|null $logger
     * @param StatsInterface|null $stats
     * @throws Exception
     */
    public function __construct(array $options = [], LoggerInterface $logger = null, StatsInterface $stats = null)
    {
        parent::__construct($options, $logger, $stats);
        try {
            $this->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            $this->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (\PDOException $e) {
            if ($e->getCode() != 'IM001') {
                $this->logger->critical($e->getMessage());
                throw new Exception($logger, $e->getMessage());
            }
        }

        if ($sth = $this->query('SELECT VERSION();')) {
            $this->version = $sth->fetchColumn();
        }
    }

    /**
     * Экранирует строки
     * @param string $s
     * @param bool $isIdentifier
     * @return string
     */
    public function escape(string $s, $isIdentifier = false): string
    {
        if (!$isIdentifier) {
            return $this->quote($s);
        } else {
            return "`" . str_replace('`', '``', $s) . "`";
        }
    }


    /**
     * @inheritDoc
     */
    public function performGetPlaceholderIgnoreRe(): string
    {
        return '';
    }

    /**
     * @param string $table
     * @param array<mixed> $data
     * @return false|int
     * @throws Exception
     * @since 1.4.0
     */
    public function upsert(string $table, array $data)
    {
        $table = $this->escape($table, true);

        $query = "INSERT INTO {$table} (?#) VALUES(?a) ON DUPLICATE KEY UPDATE ?a";


        $prepare = new Prepare($this);

        /** @var \PDOStatement|false $sth */
        $sth = $prepare->send($query, array_keys($data), array_values($data), $data)->execute();

        if ($sth === false) {
            return false;
        }

        return $sth->rowCount();
    }
}
