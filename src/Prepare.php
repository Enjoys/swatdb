<?php

declare(strict_types=1);

namespace Enjoys\SwatDB;

use Psr\Log\LoggerInterface;

/**
 * Class Prepare
 * @package Enjoys\SwatDB
 */
class Prepare
{
    private Wrapper $pdo;
    private string $query = '';
    /**
     * @var array<mixed>
     */
    private array $placeholderArgs = [];
    /**
     * @var array<int, array<mixed>>
     */
    private array $bindParams = [];
    private bool $placeholderNoValueFound = false;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * Prepare constructor.
     * @param Wrapper $pdo
     */
    public function __construct(Wrapper $pdo)
    {
        $this->pdo = $pdo;
        $this->logger = $this->pdo->getLogger();
    }

    /**
     * @param string $query
     * @param mixed ...$placeholderArgs
     * @return $this
     */
    public function send(string $query, ...$placeholderArgs): self
    {
        // table prefix replace
        $this->query = $this->replaceTablePrefix($query);
        $this->placeholderArgs = $placeholderArgs;
        return $this;
    }

    private function replaceTablePrefix(string $query): string
    {
        return str_replace('?_', $this->pdo->getTablePrefix(), $query);
    }

    /**
     * @return false|\PDOStatement
     * @throws Exception
     */
    public function execute()
    {
        $query = $this->expandPlaceholdersFlow($this->getQuery());

        /** @var \PDOStatement|false $sth */
        $sth = $this->pdo->prepare($query);

        if ($sth === false) {
            return false;
        }

        foreach ($this->bindParams as $key => $params) {
            $sth->bindValue($key + 1, $params[0], $params[1]);
        }

        $sth->execute();

        return $sth;
    }


    /**
     * Do real placeholder processing.
     * Imply that all interval variables (_placeholder_*) already prepared.
     * May be called recurrent!
     * @param string $query
     * @return string
     * @throws Exception
     */
    private function expandPlaceholdersFlow(string $query): string
    {
        $regexp = '{
            (?>
                # Ignored chunks.
                (?>
                    # Comment.
                    -- [^\r\n]*
                )
                  |
                (?>
                    # DB-specifics.
                    ' . \trim($this->pdo->performGetPlaceholderIgnoreRe()) . '
                )
            )
              |
            (?>
                # Optional blocks
                \{
                    # Use "+" here, not "*"! Else nested blocks are not processed well.
                    ( (?> (?>[^{}]+)  |  (?R) )* )             #1
                \}
            )
              |
            (?>
                # Placeholder
                (\?) ( [_dsafn\#]? )                           #2 #3
            )
        }sx';

        $query = preg_replace_callback(
            $regexp,
            [$this, 'expandPlaceholdersCallback'],
            $query
        );

        if (is_null($query)) {
            throw new Exception($this->logger, 'Error! preg_replace_callback returned null');
        }

        return $query;
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }


    /**
     * string expandPlaceholdersCallback(list $m)
     * @param array<mixed> $m
     * @return  string
     * @throws Exception
     * @see preg_replace_callback.
     */
    private function expandPlaceholdersCallback(array $m): string
    {
        // Placeholder.
        if (!empty($m[2])) {
            // Value-based placeholder.
            return $this->valueBasedPlaceholder($m[3]);
        }

        // Optional block.
        if (isset($m[1]) && strlen($m[1])) {
            return $this->expandBlock($m[1]);
        }
        // Default: skipped part of the string.
        return $m[0];
    }

    /**
     * @param string $data
     * @return string
     * @throws Exception
     */
    private function expandBlock(string $data): string
    {
        $prev = $this->placeholderNoValueFound;
        $block = $this->expandPlaceholdersFlow($data);
        $block = $this->placeholderNoValueFound ? '' : ' ' . $block . ' ';
        $this->placeholderNoValueFound = $prev; // recurrent-safe

        return $block;
    }

    /**
     * @param string $type
     * @return string
     * @throws Exception
     */
    private function valueBasedPlaceholder(string $type): string
    {
        if (!$this->placeholderArgs) {
            throw new Exception($this->logger, Wrapper::ERROR_NO_VALUE);
        }
        $value = array_shift($this->placeholderArgs);

        // Skip this value?
        if ($value === Wrapper::SKIP) {
            $this->placeholderNoValueFound = true;
            return '';
        }

        // First process guaranteed non-native placeholders.
        switch ($type) {
            //array
            case 'a':
                return $this->expandArray($value);
            // Identifier
            case "#":
                return $this->expandIdentifier($value);
            //int
            case 'd':
                return $this->expandInteger($value);
            //float
            case 'f':
                return $this->expandFloat($value);
            //string
            default:
                return $this->expandStr($value);
        }
//
//        if (false !== $key = array_search(
//                $type,
//                [
//                    'expandArray' => 'a',
//                    'expandIdentifier' => '#',
//                    'expandInteger' => 'd',
//                    'expandFloat' => 'f',
//                    'expandStr' => 's',
//                ]
//            )) {
//            return $this->$key($value);
//        }
//        return $this->expandStr($value);
    }

    /**
     * @param mixed $value
     * @return string
     * @throws Exception
     */
    private function expandArray($value): string
    {
        if (!is_array($value)) {
            throw new Exception($this->logger, Wrapper::ERROR_VALUE_NOT_ARRAY);
        }

        $parts = [];

        foreach ($value as $k => $v) {
            $this->bindParams[] = [$v, $this->getPdoParam(\gettype($v))];
            if (!is_int($k)) {
                $k = $this->pdo->escape($k, true);
                $parts[] = $k . '=?';
                continue;
            }
            $parts[] = '?';
        }

        return implode(', ', $parts);
    }

    /**
     * @param string|array<mixed> $value
     * @return string
     * @throws Exception
     */
    private function expandIdentifier($value): string
    {
        if (!is_array($value)) {
            return $this->pdo->escape($value, true);
        }

        $parts = [];
        foreach ($value as $table => $identifier) {
            if (!is_scalar($identifier)) {
                throw new Exception($this->logger, Wrapper::ERROR_VALUE_NOT_SCALAR);
            }

            $prefix = (!is_int($table) ? $this->pdo->escape($table, true) . '.' : '');
            $parts[] = $prefix . $this->pdo->escape((string)$identifier, true);
        }
        return implode(', ', $parts);
    }


    /**
     * @param mixed $value
     * @return string
     * @throws Exception
     */
    private function expandStr($value): string
    {
        if (!is_scalar($value)) {
            throw new Exception($this->logger, Wrapper::ERROR_VALUE_NOT_SCALAR);
        }
        $this->bindParams[] = [$value, $this->getPdoParam('string')];

        return '?';
    }

    /**
     * @param string|int|float $value
     * @return string
     */
    private function expandInteger($value): string
    {
        $this->bindParams[] = [$value, $this->getPdoParam('integer')];
        return '?';
    }


    /**
     * @param mixed $value
     * @return string
     */
    private function expandFloat($value): string
    {
        $this->bindParams[] = [
            (float)\str_replace(',', '.', (string)$value),
            $this->getPdoParam('string')
        ];
        return '?';
    }

    /**
     * @param string $type
     * @return int
     */
    private function getPdoParam(string $type): int
    {
        switch ($type) {
            case 'NULL':
                return \PDO::PARAM_NULL;
            case 'integer':
                return \PDO::PARAM_INT;
            case 'string':
            default:
                return \PDO::PARAM_STR;
        }
    }
}
