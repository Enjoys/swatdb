<?php

namespace Enjoys\SwatDB;

use Enjoys\SwatDB\Interfaces\StatsInterface;

/**
 * Class Stats
 * @package Enjoys\SwatDB
 */
class Stats implements StatsInterface
{

    public const QUERY_ALLTIME = 'allQueryTime';
    private static float $allQueryTime = 0.0;

    public const QUERY_COUNT = 'queryCount';
    private static int $queryCount = 0;

    public const QUERY_LIST = 'queryList';
    /**
     * @var array<string>
     */
    static array $queryList = [];

    private bool $statsLogQuery;

    public function __construct(bool $statsLogQuery = false)
    {
        $this->statsLogQuery = $statsLogQuery;
    }

    /**
     * @param string|null $key
     * @return array|false|mixed
     */
    public function getStats(string $key = null)
    {
        $data = [
            self::QUERY_ALLTIME => self::$allQueryTime,
            self::QUERY_COUNT => self::$queryCount,
            self::QUERY_LIST => self::$queryList,
        ];
        if ($key === null) {
            return $data;
        }

        if (array_key_exists($key, $data)) {
            return $data[$key];
        }

        return false;
    }

    /**
     * @param float|int $time
     */
    public function addToQueryTime($time): void
    {
        self::$allQueryTime += $time;
    }

    /**
     * @param int $count
     */
    public function addToQueryCount(int $count = 1): void
    {
        self::$queryCount += $count;
    }

    /**
     * @param string $query
     */
    public function addToQueryList(string $query): void
    {
        if ($this->statsLogQuery === true) {
            self::$queryList[] = $query;
        }
    }

    public static function clearStats(): void
    {
        self::$allQueryTime = 0.0;
        self::$queryCount = 0;
        self::$queryList = [];
    }
}
