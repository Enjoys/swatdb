<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Interfaces;

/**
 * Interface DriverInterface
 * @package Enjoys\SwatDB
 */
interface DriverInterface
{
    /**
     * string escape(mixed $s, bool $isIdent=false)
     * Enclose the string into database quotes correctly escaping
     * special characters. If $isIdentifier is true, value quoted as identifier
     * (e.g.: `value` in MySQL, "value" in Firebird, [value] in MSSQL).
     *
     * @param string $s
     * @param bool $isIdentifier
     * @return string
     */
    public function escape(string $s, bool $isIdentifier = false): string;

    /**
     * Return regular expression which matches ignored query parts.
     * This is needed to skip placeholder replacement inside comments, constants etc.
     * return ''; - simply realize
     * @return string
     */
    public function performGetPlaceholderIgnoreRe(): string;
}
