<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Interfaces;

/**
 * Interface StatsInterface
 * @package Enjoys\SwatDB
 */
interface StatsInterface
{

    /**
     * @param string|null $key
     * @return array|false|mixed
     */
    public function getStats(string $key = null);

    /**
     * @param float|int $time
     */
    public function addToQueryTime($time): void;

    /**
     * @param int $count
     */
    public function addToQueryCount(int $count = 1): void;

    /**
     * @param string $query
     */
    public function addToQueryList(string $query): void;
}
