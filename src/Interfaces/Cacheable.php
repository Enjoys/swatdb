<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Interfaces;

use Psr\SimpleCache\CacheInterface;

interface Cacheable
{
    /**
     * @param CacheInterface|null $cacher
     */
    public function setCacher(CacheInterface $cacher = null): void;

    /**
     * @return string
     */
    public function getCacheId(): string;
}
