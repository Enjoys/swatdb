<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Interfaces;

interface MethodInterface
{
    /**
     * @return mixed
     */
    public function exec();
}
