<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

/**
 * Class SelectCell
 * Return the first cell of the first column of query result.
 * If no one row selected, return null.
 * @package Enjoys\SwatDB\Methods
 */
class SelectCell extends SelectWrapper
{

    /**
     * @var string
     */
    protected string $fetchMethod = "fetchColumn";
    /**
     * @var array<int>
     */
    protected array $fetchArgs = [];
}
