<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Interfaces\MethodInterface;

/**
 * Class Truncate
 * @package Enjoys\SwatDB\Methods
 * @since 1.6.2
 */
class Truncate extends Base implements MethodInterface
{

    /**
     * @return mixed
     * @throws Exception
     */
    public function exec()
    {
        $table = array_shift($this->args);
        return $this->db->exec("TRUNCATE TABLE {$table};");
    }
}
