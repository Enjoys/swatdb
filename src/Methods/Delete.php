<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Interfaces\MethodInterface;

class Delete extends Base implements MethodInterface
{

    /**
     * @return mixed
     * @throws Exception
     */
    public function exec()
    {
        $table = array_shift($this->args);
        $where = array_shift($this->args);

        $table = $this->db->escape($table, true);


        /** @var \PDOStatement|false $sth */
        $sth = $this->prepare->send("DELETE FROM $table WHERE $where", ... $this->args)->execute();

        if ($sth === false) {
            return false;
        }

        return $sth->rowCount();
    }
}
