<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Interfaces\MethodInterface;

class Upsert extends Base implements MethodInterface
{

    /**
     * @return mixed
     * @throws Exception
     */
    public function exec()
    {
        throw new Exception($this->db->getLogger(), sprintf('Driver %s not support UPSERT', get_called_class()));
    }
}
