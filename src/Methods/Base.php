<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

use Enjoys\SwatDB\Prepare;
use Enjoys\SwatDB\Wrapper;

abstract class Base
{
    /**
     * @var array<mixed>
     */
    protected array $args;
    /**
     * @var Wrapper
     */
    protected Wrapper $db;

    protected Prepare $prepare;

    /**
     * Select constructor.
     * @param Wrapper $db
     * @param mixed ...$args
     */
    public function __construct(Wrapper $db, ...$args)
    {
        $this->db = $db;
        $this->args = $args;
        $this->prepare = new Prepare($this->db);
    }
}
