<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Interfaces\MethodInterface;

class InsertMulti extends Base implements MethodInterface
{

    /**
     * @return false|int
     * @throws Exception
     */
    public function exec()
    {
        list($table, $columns, $values) = $this->args;

        $table = $this->db->escape($table, true);

        $query = "INSERT INTO {$table} (?#) VALUES "
            . str_repeat("(?a), ", \count($values) - 1)
            . "(?a)";

        /** @var \PDOStatement|false $sth */
        $sth = $this->prepare
            ->send($query, $columns, ...$values)->execute();

        if ($sth === false) {
            return false;
        }

        return $sth->rowCount();
    }
}
