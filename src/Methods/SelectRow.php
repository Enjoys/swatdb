<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

/**
 * Class SelectRow
 * PDO::FETCH_ASSOC: возвращает массив, индексированный именами столбцов результирующего набора
 * @package Enjoys\SwatDB\Methods
 */
class SelectRow extends SelectWrapper
{
    /**
     * @var string
     */
    protected string $fetchMethod = 'fetch';
    /**
     * @var array<int>
     */
    protected array $fetchArgs = [\PDO::FETCH_ASSOC];
}
