<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

/**
 * Class SelectAll
 * PDO::FETCH_ASSOC: возвращает массив, индексированный именами столбцов результирующего набора
 * @package Enjoys\SwatDB\Methods
 */
class SelectAll extends SelectWrapper
{
    /**
     * @var string
     */
    protected string $fetchMethod = "fetchAll";
    /**
     * @var array<int>
     */
    protected array $fetchArgs = [\PDO::FETCH_ASSOC];
}
