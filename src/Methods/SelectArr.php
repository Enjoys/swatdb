<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

/**
 * Class SelectArr
 * Метод selectArr() трактует результат как
 * массив-столбец и возвращает данные в виде списка:
 * SELECT id, name FROM ?_tbl ...
 * Возвращает первое значение в виде ключа, второе в виде значения массива
 * return array(id=>name);
 * @package Enjoys\SwatDB\Methods
 */
class SelectArr extends SelectWrapper
{
    /**
     * @var string
     */
    protected string $fetchMethod = "fetchAll";
    /**
     * @var array<int>
     */
    protected array $fetchArgs = [\PDO::FETCH_KEY_PAIR];
}
