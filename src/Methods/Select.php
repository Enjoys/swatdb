<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Interfaces\MethodInterface;

class Select extends Base implements MethodInterface
{

    /**
     * @return false|\PDOStatement
     * @throws Exception
     */
    public function exec()
    {
        /** @var \PDOStatement|false $sth */
        $sth = $this->prepare->send(...$this->args)->execute();

        if ($sth === false) {
            return false;
        }
        return $sth;
    }
}
