<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

use Enjoys\SimpleCache\CacheException;
use Enjoys\SimpleCache\Cacher\NullCacher;
use Enjoys\SimpleCache\InvalidArgumentException;
use Enjoys\SwatDB\Interfaces\Cacheable;
use Enjoys\SwatDB\Interfaces\MethodInterface;
use Enjoys\SwatDB\Wrapper;
use Psr\SimpleCache\CacheInterface;

abstract class SelectWrapper extends Base implements MethodInterface, Cacheable
{
    /**
     * @since 1.2.1
     */
    public const ALGO_CACHEID = 'md5';

    /**
     * @var string
     */
    protected string $fetchMethod = '';

    /**
     * @var array<int>
     */
    protected array $fetchArgs = [];

    /**
     * @var CacheInterface
     */
    private CacheInterface $cacher;
    private string $cacheId = '';
    private ?int $cacheLifeTime = null;

    /**
     * Select constructor.
     * @param Wrapper $db
     * @param mixed ...$args
     */
    public function __construct(Wrapper $db, ...$args)
    {
        parent::__construct($db, $args);
        $this->args = $this->handlerArgs($args);
        $this->cacher = new NullCacher();
    }

    /**
     * @return mixed
     * @throws CacheException
     * @throws InvalidArgumentException
     */
    public function exec()
    {
        //get cache or result with save
        return $this->cacher->get(
            $this->cacheId,
            function () {
                if (false === $sth = (new Select($this->db, ...$this->args))->exec()) {
                    return false;
                }

                //get result
                $result = $sth->{$this->fetchMethod}(...$this->fetchArgs);

                //save result to cache
                if ($this->cacheLifeTime !== null) {
                    $this->cacher->set($this->cacheId, $result, $this->cacheLifeTime);
                }
                return $result;
            }
        );
    }

    /**
     * @param CacheInterface|null $cacher
     */
    public function setCacher(CacheInterface $cacher = null): void
    {
        $this->cacher = $cacher ?? new NullCacher();
    }

    /**
     * @param mixed $args
     * @return array<mixed>
     * @since 1.1.0
     */
    private function handlerArgs($args): array
    {
        $this->resetCacheInfo();
        if (is_numeric($args[0])) {
            $this->cacheId = $this->createCacheId(getCallingFunctionName()['function'] . json_encode($args));
            $this->cacheLifeTime = (int)array_shift(
                $args
            );
        }

        return $args;
    }

    /**
     * @since 1.1.0
     */
    private function resetCacheInfo(): void
    {
        $this->cacheId = '';
        $this->cacheLifeTime = null;
    }

    /**
     * @param string $string
     * @return string
     * @since 1.1.0 Добавлено кеширование
     * @since 1.2.1 Добавлена опция для изменения алгоритма хэширования algoCacheId, по умолчанию используется md5. Раньше был только  md5
     * например ['algoCacheId' => 'haval160,4'] вместо md5
     */
    private function createCacheId(string $string): string
    {
        return hash($this->db->getOption('algoCacheId', self::ALGO_CACHEID), $string);
    }

    /**
     * @return string
     */
    public function getCacheId(): string
    {
        return $this->cacheId;
    }
}
