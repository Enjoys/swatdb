<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

/**
 * Class SelectCol
 * Метод selectCol() трактует результат как
 * массив-столбец и возвращает данные в виде списка:
 * Return the first column of query result as array.
 * PDO::FETCH_COLUMN: Будет возвращен указанный столбец. Индексация столбцов начинается с 0.
 * @package Enjoys\SwatDB\Methods
 */
class SelectCol extends SelectWrapper
{
    /**
     * @var string
     */
    protected string $fetchMethod = 'fetchAll';
    /**
     * @var array<int>
     */
    protected array $fetchArgs = [\PDO::FETCH_COLUMN];
}
