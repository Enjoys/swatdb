<?php

declare(strict_types=1);

namespace Enjoys\SwatDB\Methods;

use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Interfaces\MethodInterface;

class Insert extends Base implements MethodInterface
{

    /**
     * @return false|int
     * @throws Exception
     */
    public function exec()
    {
        list($table, $data) = $this->args;

        $table = $this->db->escape($table, true);

        /** @var \PDOStatement|false $sth */
        $sth = $this->prepare
            ->send(
                "INSERT INTO {$table} (?#) VALUES(?a)",
                array_keys($data),
                array_values($data),
            )->execute();

        if ($sth === false) {
            return false;
        }

        return $sth->rowCount();
    }
}
