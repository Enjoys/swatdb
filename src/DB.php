<?php

declare(strict_types=1);

namespace Enjoys\SwatDB;

use Enjoys\SwatDB\Interfaces\StatsInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class DB
 * @package Enjoys\SwatDB
 */
class DB
{
    /**
     * @param array<mixed> $options
     * @param LoggerInterface|null $logger
     * @param StatsInterface|null $stats
     * @return Wrapper
     * @throws Exception
     */
    public static function connect(
        array $options = [],
        LoggerInterface $logger = null,
        StatsInterface $stats = null
    ): Wrapper {
        $logger ??= new NullLogger();

        if (!isset($options['dsn'])) {
            throw new Exception($logger, 'DSN don\'t set');
        }

        $scheme = parse_url($options['dsn'], PHP_URL_SCHEME);

        if (!is_string($scheme)) {
            throw new Exception($logger, 'Invalid DSN');
        }

        $class = '\Enjoys\SwatDB\Drivers\\' . ucfirst($scheme);
        // Load database driver and create its instance.
        if (class_exists($class)) {
            $object = new $class($options, $logger, $stats);
            if ($object instanceof Wrapper) {
                return $object;
            }
        }
        throw new Exception($logger, sprintf('Driver %s not support', $class));
    }
}
