<?php

declare(strict_types=1);

namespace Enjoys\SwatDB;

use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Throwable;

/**
 * Class Exception
 * @package Enjoys\SwatDB
 */
class Exception extends \Exception
{
    use LoggerAwareTrait;

    /**
     * Exception constructor.
     * @param LoggerInterface|null $logger
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(LoggerInterface $logger = null, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->logger = $logger ?? new NullLogger();
        parent::__construct($message, $code, $previous);

        $this->logger->critical(
            $message,
            [
                'code' => $this->getCode(),
                'file' => $this->getFile(),
                'line' => $this->getLine(),
                'trace' => $this->getTrace(),
                'server' => $_SERVER,
            ]
        );
    }
}
