<?php

declare(strict_types=1);

namespace Enjoys\SwatDB;

use Enjoys\SwatDB\Interfaces\StatsInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Statement
 * @package Enjoys\SwatDB
 */
class Statement extends \PDOStatement
{

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var StatsInterface
     */
    private StatsInterface $stats;


    protected function __construct(LoggerInterface $logger, StatsInterface $stats)
    {
        $this->logger = $logger;
        $this->stats = $stats;
    }


    /**
     * @param bool $addToQueryTime
     * @param bool $addToQueryCount
     * @param string $logLevel
     * @param string $method
     * @param mixed ...$arguments
     * @return mixed
     */
    private function wrap(
        string $method,
        string $logLevel = 'debug',
        $addToQueryTime = true,
        $addToQueryCount = true,
        ...$arguments
    ) {
        $start_time = microtime(true);
        $result = parent::$method(...$arguments);
        $work_time = microtime(true) - $start_time;

        $query = $this->queryString;

        $this->logger->$logLevel(
            '[' . $work_time . '] PDOStatement::' . $method . ' - ' . $query,
            [
                'queryString' => $query,
                'arguments' => $arguments,
                'time' => $work_time,
            ]
        );

        if ($addToQueryTime) {
            $this->stats->addToQueryTime($work_time);
        }

        if ($addToQueryCount) {
            $this->stats->addToQueryCount();
            $this->stats->addToQueryList($query);
        }

        return $result;
    }

    #[\ReturnTypeWillChange]
    /**
     * @inheritDoc
     *
     * @param mixed $column
     * @param mixed $var
     * @param int|null $type
     * @param int|null $maxLength
     * @param mixed $driverOptions
     * @return bool
     */
    public function bindColumn($column, &$var, $type = null, $maxLength = null, $driverOptions = null)
    {
        return $this->wrap('bindColumn', 'debug', true, false, ...func_get_args());
    }

    #[\ReturnTypeWillChange]
    /**
     * @inheritDoc
     *
     * @param mixed $param
     * @param mixed $value
     * @param int $type
     * @return bool
     */
    public function bindValue($param, $value, $type = \PDO::PARAM_STR)
    {
        return $this->wrap('bindValue', 'debug', true, false, ...func_get_args());
    }

    #[\ReturnTypeWillChange]
    /**
     * @inheritDoc
     *
     * @param mixed $param
     * @param mixed $var
     * @param int $type
     * @param int|null $maxLength
     * @param mixed $driverOptions
     * @return bool
     */
    public function bindParam($param, &$var, $type = \PDO::PARAM_STR, $maxLength = null, $driverOptions = null)
    {
        return $this->wrap('bindParam', 'debug', true, false, ...func_get_args());
    }

    #[\ReturnTypeWillChange]
    /**
     * @inheritDoc
     *
     * @param array<mixed>|null $params
     * @return bool
     */
    public function execute($params = null)
    {
        return $this->wrap('execute', 'info', true, true, ...func_get_args());
    }

    #[\ReturnTypeWillChange]
    /**
     * @inheritDoc
     *
     * @param int|null $mode
     * @param int $cursorOrientation
     * @param int $cursorOffset
     * @return mixed
     */
    public function fetch($mode = null, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
        return $this->wrap('fetch', 'debug', true, false, ...func_get_args());
    }

    #[\ReturnTypeWillChange]
    /**
     * @param int $mode
     * @param mixed ...$args
     * @return array|false|mixed
     */
    public function fetchAll($mode = \PDO::FETCH_BOTH, ...$args)
    {
        return $this->wrap('fetchAll', 'debug', true, false, ...func_get_args());
    }

    #[\ReturnTypeWillChange]
    /**
     * @inheritDoc
     *
     * @param int $column
     * @return mixed
     */
    public function fetchColumn($column = 0)
    {
        return $this->wrap('fetchColumn', 'debug', true, false, ...func_get_args());
    }

    #[\ReturnTypeWillChange]
    /**
     * @inheritDoc
     *
     * @param string|null $class_name
     * @param array<mixed>|null $ctor_args
     * @return mixed
     */
    public function fetchObject($class_name = null, $ctor_args = null)
    {
        return $this->wrap('fetchObject', 'debug', true, false, ...func_get_args());
    }
}
