<?php

namespace Tests\Enjoys\SwatDB;

use Enjoys\SimpleCache\CacheException;
use Enjoys\SimpleCache\Cacher\FileCache;
use Enjoys\SimpleCache\InvalidArgumentException;
use Enjoys\SwatDB\DB;
use Enjoys\SwatDB\Drivers\Mysql;
use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Statement;
use Enjoys\SwatDB\Stats;
use Enjoys\SwatDB\StatsInterface;
use PHPUnit\Framework\TestCase;

class WrapperTest extends TestCase
{
    use ReflectionTrait;


    /**
     * @throws Exception
     */
    public function testGetStats(): void
    {
        $db = DB::connect(['dsn' => 'sqlite:memory']);
        $this->assertArrayHasKey(Stats::QUERY_ALLTIME, $db->getStats());
        $this->assertArrayHasKey(Stats::QUERY_COUNT, $db->getStats());
    }

    /**
     * @throws Exception
     */
    public function testGetTablePrefix(): void
    {
        $db = DB::connect(['dsn' => 'sqlite:memory']);
        $this->assertSame('', $db->getTablePrefix());
        $db->setOption('tablePrefix', 'test_');
        $this->assertSame('test_', $db->getTablePrefix());
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws CacheException
     */
    public function testCacheSelect(): void
    {
        $cacher = new FileCache();
        $cacher->clear();
        $db = DB::connect(['dsn' => 'sqlite:memory', 'cacher' => $cacher]);
        $db->exec(
        /** @lang SQLite */            "
DROP TABLE IF EXISTS test_cache_select;
CREATE TABLE test_cache_select(
    `id`        INTEGER  PRIMARY KEY AUTOINCREMENT,
    `val`     TEXT   NULL
); "
        );

        $db->insert('test_cache_select', ['val' => 5]);
        $id = $db->lastInsertId();
        $lifetime = 300;
        $this->assertSame('5', $db->selectCell($lifetime, "SELECT val FROM test_cache_select WHERE id = ?d", $id));

        $cacheId = $db->getCacheId();
        $db->update('test_cache_select', ['val' => 10], 'id = ?d', $id);
        $this->assertSame('5', $db->selectCell($lifetime, "SELECT val FROM test_cache_select WHERE id = ?d", $id));
        $cacher->delete($cacheId);
        $this->assertSame('10', $db->selectCell($lifetime, "SELECT val FROM test_cache_select WHERE id = ?d", $id));
    }

    /**
     * @throws Exception
     */
    public function testSetPdoAttributes(): void
    {
        $db = DB::connect(['dsn' => 'sqlite:memory']);
        $this->assertSame(\PDO::ERRMODE_EXCEPTION, $db->getAttribute(\PDO::ATTR_ERRMODE));
        $this->assertSame(Statement::class, $db->getAttribute(\PDO::ATTR_STATEMENT_CLASS)[0]);
        $db = null;

        $db = DB::connect(
            [
                'dsn' => 'sqlite:memory',
                'pdoOptions' => [
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_SILENT,
                    \PDO::ATTR_STATEMENT_CLASS => [\PDOStatement::class]
                ]
            ]
        );
        $this->assertSame(\PDO::ERRMODE_SILENT, $db->getAttribute(\PDO::ATTR_ERRMODE));
        $this->assertSame(\PDOStatement::class, $db->getAttribute(\PDO::ATTR_STATEMENT_CLASS)[0]);
    }

    public function testInvalidConnection(): void
    {
        $this->expectException(Exception::class);
        new Mysql();
    }
}
