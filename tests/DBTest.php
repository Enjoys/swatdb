<?php

namespace Tests\Enjoys\SwatDB;

use Enjoys\SwatDB\DB;
use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Wrapper;
use PHPUnit\Framework\TestCase;

class DBTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testConnect_success(): void
    {
        $connect = DB::connect(['dsn' => 'sqlite:memory']);
        $this->assertInstanceOf(Wrapper::class, $connect);
    }

    public function testConnect_fail(): void
    {
        $this->expectException(Exception::class);
        DB::connect(['invalid_DSN']);
    }

    public function testInvalidDsn()
    {
        $this->expectException(Exception::class);
        DB::connect(['dsn' => 'invalid_dsn']);
    }

    public function testInvalidDriver()
    {
        $this->expectException(Exception::class);
        DB::connect(['dsn' => 'http:invalid']);
    }
}
