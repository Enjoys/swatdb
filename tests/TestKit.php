<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace Tests\Enjoys\SwatDB;

use Enjoys\SimpleCache\InvalidArgumentException;
use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Wrapper;
use PHPUnit\Framework\TestCase;

class TestKit extends TestCase
{
    protected Wrapper $db;
    protected string $dbScheme;
    protected string $tablePrefix = 'test_';
/**
     * @return array<mixed>
     */
    protected function data(): array
    {
        return include __DIR__ . '/fixtures/insert_data.php';
    }


    /**
     * @return mixed
     */
    public function expectedTestInsert()
    {
        $data = $this->data()['first_query'];
        $data['enum_'] = (string)$data['enum_'];
        return $data;
    }


    /**
     * @depends testConnect
     * @param Wrapper $db
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function testInsert(Wrapper $db): void
    {
        $data = $this->data()['first_query'];
        $count = $db->insert('?_phpunit', $data);
        $this->assertSame(1, $count);
//PDO::lastInsertId() return string
        $lastInsertId = $db->lastInsertId();
        $this->assertSame('1', $lastInsertId);
        $result = $db->selectRow("SELECT * FROM ?_phpunit WHERE id = ?d", $lastInsertId);
        unset($result['id']);
        $this->assertSame($this->expectedTestInsert(), $result);
    }


    /**
     * @var array<mixed>
     */
    protected array $expectedInsertMultiWithNullAndFloat = [

        'text_' => 'test_text3',
        'float_' => 2.5222,
        'double_' => 3.14123456,
        'enum_' => '2',
        'date_' => null,

    ];
/**
     * @depends testConnect
     * @param Wrapper $db
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function testInsertMultiWithNullAndFloat(Wrapper $db): void
    {
        $data = $this->data()['multi'];
        $count = $db->insertMulti('?_phpunit', $data['columns'], $data['data']);
        $this->assertSame(3, $count);
        $this->assertSame(['multi1', 'multi2', 'multi3'], $db->selectCol("SELECT text_ FROM ?_phpunit WHERE text_ LIKE ?", 'multi%'));
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     * @throws InvalidArgumentException
     */
    public function testSelectAll(Wrapper $db): void
    {
        $this->assertSame([
                ['text_' => 'test_text'],
                ['text_' => 'multi1'],
                ['text_' => 'multi2'],
                ['text_' => 'multi3'],
            ], $db->selectAll("SELECT text_ FROM ?_phpunit"));
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     * @throws InvalidArgumentException
     */
    public function testSelectWithIdentifierPlaceholder(Wrapper $db): void
    {
        $this->assertSame([
                '2020-12-22',
                '2020-10-01',
                null,
                '2020-10-03',
            ], $db->selectCol("SELECT ?# FROM ?_phpunit", ['date_']));
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     * @throws InvalidArgumentException
     */
    public function testSelectWithSkip(Wrapper $db): void
    {
        $this->assertSame([
                '2020-12-22',
                '2020-10-01',
                null,
                '2020-10-03',
            ], $db->selectCol("SELECT date_ FROM ?_phpunit WHERE 1 = 1 { AND id = ?d }", Wrapper::SKIP));
        $this->assertSame([
                '2020-10-01'
            ], $db->selectCol("SELECT ?# FROM ?_phpunit WHERE 1 = 1 { AND id = ?d }", ['date_'], 2));
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     * @throws InvalidArgumentException
     */
    public function testSelectArr(Wrapper $db): void
    {
        $this->assertSame([
                'multi1' => '2020-11-01 12:30:00',
                'multi2' => '2020-11-02 12:30:00',
                'multi3' => '2020-11-03 12:30:00'
            ], $db->selectArr("SELECT text_, datetime_ FROM ?_phpunit WHERE id > 1"));
    }

    /**
     * @var array|int[]
     */
    protected array $expectedSelectCol = [10, 120];
/**
     * @depends testConnect
     * @param Wrapper $db
     * @throws InvalidArgumentException
     */
    public function testSelectCol(Wrapper $db): void
    {
        $this->assertSame($this->expectedSelectCol, $db->selectCol("SELECT integer_ FROM ?_phpunit WHERE id IN (?a)", [1, 3]));
    }

    /**
     * @var float|string
     */
    protected $expectedTestUpdate = 100.5;
/**
     * @depends testConnect
     * @param Wrapper $db
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function testUpdate(Wrapper $db): void
    {
        $db->update('?_phpunit', ['double_' => 100.500], 'id = ?d', 2);
        $this->assertSame($this->expectedTestUpdate, $db->selectCell("SELECT double_ FROM ?_phpunit WHERE id = ?d", '2'));
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function testDelete(Wrapper $db): void
    {
        $db->delete('?_phpunit', 'id = ?d', 1);
        $this->assertSame(false, $db->selectCell("SELECT text_ FROM ?_phpunit WHERE id = ?d", '1'));
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     * @throws Exception
     */
    public function testSimpleUpsert(Wrapper $db): void
    {
        $data = [
            'id' => 1,
            'name' => 'test'
        ];
        $db->upsert('?_upsert', $data);
        $this->assertSame('test', $db->selectCell("SELECT name FROM ?_upsert WHERE id = ?d", 1));
        $data = [
            'id' => 1,
            'name' => 'test2'
        ];
        $db->upsert('?_upsert', $data);
        $this->assertSame('test2', $db->selectCell("SELECT name FROM ?_upsert WHERE id = ?d", 1));
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     */
    public function testPDO(Wrapper $db): void
    {
        $sth = $db->prepare('SELECT enum_ FROM test_phpunit WHERE date_ > ? AND float_ < ?');
        $sth->execute(array('2020-09-30', 1.122));
        $result1 = $sth->fetchAll(\PDO::FETCH_NUM);
        $sth->execute(array('2020-09-30', 1.2));
        $result2 = $sth->fetchAll(\PDO::FETCH_NUM);
        $this->assertSame([['2']], $result1);
        $this->assertSame([['2'], ['2']], $result2);
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     */
    public function testPDO2(Wrapper $db): void
    {
        $enum = 1;
        $text = 'multi';
        $sth = $db->prepare('SELECT datetime_ FROM test_phpunit WHERE enum_ = :enum AND text_ LIKE :text');
        $sth->bindParam(':enum', $enum, \PDO::PARAM_INT);
        $sth->bindValue(':text', "{$text}%");
        $sth->execute();
        $this->assertSame([['2020-11-02 12:30:00']], $sth->fetchAll(\PDO::FETCH_NUM));
    }
    /**
     * @depends testConnect
     * @param Wrapper $db
     */
    public function testTransaction(Wrapper $db): void
    {
        try {
            $db->beginTransaction();
            $db->insert('?_phpunit', ['text_' => 1]);
            $this->assertSame(4, (int)$db->selectCell("SELECT COUNT(*) FROM ?_phpunit"));
            $db->insert('?_phpunit', ['fake_column' => 1]);
            $db->commit();
        } catch (Exception $e) {
            $this->assertSame(3, (int) $db->selectCell("SELECT COUNT(*) FROM ?_phpunit"));
        }
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     */
    public function testTruncate(Wrapper $db): void
    {
        $result = $db->selectAll('SELECT * FROM ?_phpunit');
        $this->assertNotEmpty($result);
        $db->truncate('?_phpunit');
        $result = $db->selectAll('SELECT * FROM ?_phpunit');
        $this->assertSame([], $result);
    }
}
