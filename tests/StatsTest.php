<?php

namespace Tests\Enjoys\SwatDB;

use Enjoys\SwatDB\Stats;
use PHPUnit\Framework\TestCase;

class StatsTest extends TestCase
{


    public function testAddToQueryList(): void
    {
        Stats::clearStats();

        $stats = new Stats(true);
        $stats->addToQueryList('1');
        $stats->addToQueryList('2');
        $stats->addToQueryList('3');
        $this->assertSame(['1', '2', '3'], $stats->getStats(Stats::QUERY_LIST));
    }


    public function testAddToQueryTime(): void
    {
        $stats = new Stats();
        $stats->addToQueryTime(1.2);
        $stats->addToQueryTime(2.9);
        $this->assertSame(4.1, $stats->getStats(Stats::QUERY_ALLTIME));
    }

    public function testAddToQueryCount(): void
    {
        $stats = new Stats();
        $stats->addToQueryCount();
        $stats->addToQueryCount(2);
        $this->assertSame(3, $stats->getStats(Stats::QUERY_COUNT));
    }

    public function testGetStatsAll(): void
    {
        $stats = new Stats();
        $this->assertSame(
            [
                Stats::QUERY_ALLTIME => 4.1,
                Stats::QUERY_COUNT => 3,
                Stats::QUERY_LIST => [
                    '1',
                    '2',
                    '3'
                ]
            ],
            $stats->getStats()
        );
    }

    public function testClearStats(): void
    {
        Stats::clearStats();
        $stats = new Stats();
        $this->assertSame(
            [
                Stats::QUERY_ALLTIME => 0.0,
                Stats::QUERY_COUNT => 0,
                Stats::QUERY_LIST => []
            ],
            $stats->getStats()
        );
    }
}
