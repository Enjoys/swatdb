<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace Tests\Enjoys\SwatDB\Drivers;

use Enjoys\SimpleCache\InvalidArgumentException;
use Enjoys\SwatDB\DB;
use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Stats;
use Enjoys\SwatDB\Wrapper;
use Tests\Enjoys\SwatDB\TestKit;

/**
 * Class SqliteTest
 * @package Tests\Enjoys\SwatDB\Drivers
 *
 * _requires extension pdo-sqlite
 */
class SqliteTest extends TestKit
{


    public static function setUpBeforeClass(): void
    {
        Stats::clearStats();
    }

    protected function setUp(): void
    {
        $this->dbScheme = (string)file_get_contents(__DIR__ . '/../fixtures/sqlite.sql');
    }

    public function testConnect(): Wrapper
    {
        $db = DB::connect([
                'dsn' => 'sqlite:memory',
                'tablePrefix' => $this->tablePrefix,
            ]);
        $this->assertInstanceOf(Wrapper::class, $db);
        return $db;
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function testImportDBScheme(Wrapper $db): void
    {
        $db->exec($this->dbScheme);
        $this->assertSame($this->tablePrefix . 'phpunit', $db->selectCell("SELECT name FROM sqlite_master WHERE type='table' AND name LIKE ? ;", '%phpunit%'));
    }


    /**
     * @return mixed
     */
    public function expectedTestInsert()
    {
        $data = $this->data()['first_query'];
        foreach ($data as $key => $item) {
            $data[$key] = (string)$item;
        }
        return $data;
    }

    /**
     * @var array<mixed>
     */
    protected array $expectedInsertMultiWithNullAndFloat = [

        'text_' => 'test_text3',
        'float_' => '2.5222',
        'double_' => '3.14123456',
        'enum_' => '2',
        'date_' => null,

    ];
/**
     * @var array|string[]
     */
    protected array $expectedSelectCol = ['10', '120'];
/**
     * @var float|string
     */
    protected $expectedTestUpdate = '100.5';
}
