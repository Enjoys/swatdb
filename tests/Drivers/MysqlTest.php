<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace Tests\Enjoys\SwatDB\Drivers;

use Enjoys\SwatDB\DB;
use Enjoys\SwatDB\Exception;
use Enjoys\SwatDB\Stats;
use Enjoys\SwatDB\Wrapper;
use Tests\Enjoys\SwatDB\TestKit;

/**
 * Class MysqlTest
 * @package Tests\Enjoys\SwatDB\Drivers
 *
 * _requires extension pdo-mysql
 */
class MysqlTest extends TestKit
{


    public static function setUpBeforeClass(): void
    {
        Stats::clearStats();
    }

    protected function setUp(): void
    {
        $this->dbScheme = (string)file_get_contents(__DIR__ . '/../fixtures/mysql.sql');
    }

    /**
     * @return Wrapper
     * @throws Exception
     */
    public function testConnect(): Wrapper
    {
        /** @var array<mixed> $setting */
        $setting = parse_ini_file(__DIR__ . '/../.env');
        $db = DB::connect(
            [
                'dsn' => $setting['MYSQL_DSN'],
                'username' => $setting['MYSQL_USER'],
                'password' => $setting['MYSQL_ROOT_PASSWORD'],
                'tablePrefix' => $this->tablePrefix,
            ]
        );

        $db->exec(/** @lang MySQL */ "CREATE DATABASE IF NOT EXISTS  phpunit;");
        $db->exec(/** @lang MySQL */ "USE phpunit;");

        $this->assertInstanceOf(Wrapper::class, $db);

        return $db;
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     * @throws Exception
     */
    public function testCreateDatabase(Wrapper $db): void
    {
        $db->exec(/** @lang MySQL */ "CREATE DATABASE IF NOT EXISTS phpunit_temp;");
        $sth = $db->query(/** @lang MySQL */ "SHOW DATABASES LIKE '%phpunit_temp%'");
        if ($sth === false) {
            $result = false;
        } else {
            $result = $sth->fetchColumn();
        }
        $this->assertSame(
            'phpunit_temp',
            $result
        );
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     * @throws Exception
     */
    public function testDeleteDatabase(Wrapper $db): void
    {
        $db->exec(/** @lang MySQL */ "DROP DATABASE IF EXISTS phpunit_temp;");
        $sth = $db->query(/** @lang MySQL */ "SHOW DATABASES LIKE '%phpunit_temp%'");
        if ($sth === false) {
            $result = false;
        } else {
            $result = $sth->fetchColumn();
        }
        $this->assertSame(
            false,
            $result
        );
    }

    /**
     * @depends testConnect
     * @param Wrapper $db
     * @throws Exception
     */
    public function testImportDBScheme(Wrapper $db): void
    {
        $db->exec($this->dbScheme);
        $sth = $db->query("SHOW TABLES LIKE '%phpunit%'");
        if ($sth === false) {
            $result = false;
        } else {
            $result = $sth->fetchColumn();
        }
        $this->assertSame(
            $this->tablePrefix . 'phpunit',
            $result
        );
    }
}
