<?php

return [
    'first_query' => [
        'text_' => 'test_text',
        'integer_' => 10,
        'float_' => 2.5,
        'double_' => 3.2,
        'date_' => '2020-12-22',
        'datetime_' => '2020-12-23 15:30:22',
        'enum_' => 2,
    ],
    'mass_insert_query' => [
        [
            'text_' => 'test_text2',
            'integer_' => 20,
            'float_' => 2.5,
            'double_' => 3.2,
            'date_' => '2020-12-02',
            'datetime_' => '2020-12-03 15:30:22',
            'enum_' => 1,
        ],
        [
            'text_' => 'test_text3',
            'integer_' => 30,
            'float_' => 2.5222,
            'double_' => 3.14123456,
            'date_' => null,
            'datetime_' => '2020-11-03 15:30:22',
            'enum_' => '2',
        ],
        [
            'text_' => 'test_text4',
            'integer_' => 40,
            'float_' => 1.2,
            'double_' => 3.14123456,
            'date_' => '2020-10-01',
            'datetime_' => '2020-11-03 15:30:22',
            'enum_' => '2',
        ],
    ],
    'multi' => [
        'columns' => [
            'text_',
            'integer_',
            'float_',
            'double_',
            'date_',
            'datetime_',
            'enum_',
        ],
        'data' => [
            [
                'multi1',
                110,
                1.121,
                3.141,
                '2020-10-01',
                '2020-11-01 12:30:00',
                2,
            ],
            [
                'multi2',
                120,
                1.122,
                3.142,
                null,
                '2020-11-02 12:30:00',
                1,
            ],
            [
                'multi3',
                130,
                1.123,
                3.143,
                '2020-10-03',
                '2020-11-03 12:30:00',
                2,
            ],
        ]
    ]

];
