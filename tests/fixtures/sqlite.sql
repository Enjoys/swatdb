DROP TABLE IF EXISTS test_phpunit;
CREATE TABLE test_phpunit
(
    `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
    `text_`     TEXT(50)   NULL,
    `integer_`  INTEGER(5) NULL,
    `float_`    REAL       NULL,
    `double_`   REAL       NULL,
    `date_`     TEXT(10)   NULL,
    `datetime_` TEXT(10)   NULL,
    `enum_`     TEXT(2)
);

DROP TABLE IF EXISTS test_upsert;
CREATE TABLE test_upsert
(
    `id`   INTEGER PRIMARY KEY AUTOINCREMENT,
    `name` VARCHAR(50) NULL
);