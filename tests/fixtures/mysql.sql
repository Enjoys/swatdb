DROP TABLE IF EXISTS test_phpunit;
CREATE TABLE test_phpunit
(
    `id`        INT            NOT NULL AUTO_INCREMENT,
    `text_`     VARCHAR(50)    NULL,
    `integer_`  INT(5)         NULL,
    `float_`    FLOAT          NULL,
    `double_`   DOUBLE         NULL,
    `date_`     DATE           NULL,
    `datetime_` DATETIME       NULL,
    `enum_`     ENUM ('1','2') NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS test_upsert;
CREATE TABLE test_upsert
(
    `id`   INT         NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;